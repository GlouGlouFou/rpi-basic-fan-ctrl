# Basic fan control for Raspberry pi

A simple bash script providing basic ON/OFF fan control for raspberry pi, triggered based on CPU temperature. No dependency required. Tested on Ubuntu 20.04.

# Usage
The scipt needs to execute as root to access GPIOs, it can be called periodically with a cron job:

Copy the script anywhere you want, example:

```
sudo cp fan-control.sh /root/fan-control.sh
```

Make the script executable:

```bash
sudo chmod +x /root/fan-control.sh
```

Run the scipt automatical every minute by adding it to root's cron tasks:

```bash
sudo crontab -e
```

and add the following job:

```bash
# Fan control based on CPU temp every minute
* * * * * /root/fan-control.sh
```

# Configuration
Edit the script `/root/fan-control.sh` and modify the following parameters to your liking:

```bash
## CONFIG
onTemp=70.0     #Start fan when CPU temp > 70 degC
offTemp=50.0    #Stop fan when CPU temp < 50 degC
pin=17          #Use GPIO pin BCM 17 to control the fan
## END CONFIG
```
