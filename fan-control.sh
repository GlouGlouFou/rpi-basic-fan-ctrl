#!/bin/bash
# Source: https://github.com/JeremySCook/RaspberryPi-Fan-Control
# GPIO handling: https://raspberrypi-aa.github.io/session2/bash.html
# Modified by Antoine RAYNARD on 2021-04-24
# Work for Ubuntu 20.04 Raspberry Pi edition

## CONFIG
onTemp=70.0     #Start fan when CPU temp > 70 degC
offTemp=50.0    #Stop fan when CPU temp < 50 degC
pin=17          #Use GPIO pin BCM 17 to control the fan
## END CONFIG

# Exports pin to userspace
echo $pin > /sys/class/gpio/export
# Sets pin as an output
echo "out" > /sys/class/gpio/gpio$pin/direction
# Read CPU temperature
temp=$(cat /sys/class/thermal/thermal_zone*/temp | sed 's/\(.\)..$/.\1/')
echo $temp

if (( $(echo "$temp > $onTemp" |bc -l) )); then
	echo "Fan ON"
	echo "1" > /sys/class/gpio/gpio$pin/value
fi
if (( $(echo "$temp < $offTemp" |bc -l) )); then
	echo "Fan OFF"
	echo "0" > /sys/class/gpio/gpio$pin/value
fi
